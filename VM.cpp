#include "VM.h"
#include "cop.h"
#include <iostream>
#include <sstream>

const yla_cop_type VM::MAGIC[] = {0xde, 0xad, 0xc0, 0xde};

VM::VM() 
{
    setError(NO_CODE);
}
VM::VM(program loaded_program_to_use)
{
    if (loaded_program_to_use.size() == 0)
    {
        setError(NO_CODE);
    } else {
        loaded_program = loaded_program_to_use;
        if (!checkHeader())
        {
            setError(WRONG_HEADER);
        }
    }
}
yla_cop_type VM::getCommand()
{
    if (pc + 1 > loaded_program.size())
    {
        setError(PROGRAM_EXCEEDED);
        return CNOP;
    }
    return loaded_program[pc++];
}
// checks first 4 bytes against provided constants
bool VM::checkHeader()
{
    for (auto i = 0; i < MAGIC_SIZE; i++)
    {
        if (getCommand() != MAGIC[i])
        {
            return false;
        }
    }
    return true;
}
// sets error code
void VM::setError(int code)
{
    _lastError = code;
}
int VM::run()
{
    int cmd_res;
    
    for(;;)
    {
        if (pc + 1 > loaded_program.size())
        {
            setError(PROGRAM_EXCEEDED);
            return 0;
        }
        
        cmd_res = doCommand();
        
        if (cmd_res == -1)
        {
            return 1;
        }
        if (cmd_res == 0)
        {
            return 0;
        }
    }
}
int VM::doCommand()
{
    if (pc + 1 > loaded_program.size())
    {
        setError(PROGRAM_EXCEEDED);
        return 0;
    }
    return doCommandInternal(getCommand());
}
int VM::doCommandInternal(yla_cop_type cop)
{
    if (_lastError != NO_ERROR)
    {
        return 0;
    }
    switch(cop) 
    {
        case CNOP:
            return 1;
        case CHALT:
            return -1;
        case CPUSH:
        {
            double value = loadValue();
            main_stack.push(makeDouble(value));
            return 1;
        }
        case CPUSHV:
        {
            array value = loadVector();
            main_stack.push(makeVector(value));
            return 1;
        }
        case COUT:
        {
            if (main_stack.empty()) {
                setError(STACK_PULL_FAIL);
                return 0;
            }
            myStackItem top = main_stack.top();
            if (isDouble(top)) {
                _lastOutput = formatNumber(getDouble(top));
            } else {
                _lastOutput = formatArray(getVector(top));
            }
            return 1;
        }
        case CMUL:
        {
            if (main_stack.empty()) {
                setError(STACK_PULL_FAIL);
		return 0;
            }
            myStackItem a = main_stack.top();
            main_stack.pop();

            if (main_stack.empty()) {
                setError(STACK_PULL_FAIL);
		return 0;
            }
            myStackItem b = main_stack.top();
            main_stack.pop();

            if (!isDouble(a) || !isDouble(b)) {
                setError(TYPE_MISMATCH);
		return 0;
            }
		
            main_stack.push(makeDouble(getDouble(b) * getDouble(a)));
	    return 1;
        }
        case CSUB:
        {
            if (main_stack.empty()) {
                setError(STACK_PULL_FAIL);
		return 0;
            }
            myStackItem a = main_stack.top();
            main_stack.pop();

            if (main_stack.empty()) {
                setError(STACK_PULL_FAIL);
		return 0;
            }
            myStackItem b = main_stack.top();
            main_stack.pop();

            if (!isDouble(a) || !isDouble(b)) {
                setError(TYPE_MISMATCH);
		return 0;
            }
		
            main_stack.push(makeDouble(getDouble(b) - getDouble(a)));
	    return 1;
        }
        case CDIV:
        {
            if (main_stack.empty()) {
                setError(STACK_PULL_FAIL);
		return 0;
            }
            myStackItem a = main_stack.top();
            main_stack.pop();

            if (main_stack.empty()) {
                setError(STACK_PULL_FAIL);
		return 0;
            }
            myStackItem b = main_stack.top();
            main_stack.pop();

            if (!isDouble(a) || !isDouble(b)) {
                setError(TYPE_MISMATCH);
		return 0;
            }
            
            if (getDouble(a) == 0) {
                setError(DIVISION_BY_ZERO);
                return 0;
            }
		
            main_stack.push(makeDouble(getDouble(b) / getDouble(a)));
	    return 1;
        }
        case CADD:
        {
            if (main_stack.empty()) {
                setError(STACK_PULL_FAIL);
            	return 0;
            }
            myStackItem a = main_stack.top();
            main_stack.pop();

            if (main_stack.empty()) {
                setError(STACK_PULL_FAIL);
            	return 0;
            }
            myStackItem b = main_stack.top();
            main_stack.pop();
		
            if (isDouble(b) && isDouble(a)) {
		main_stack.push(makeDouble(getDouble(b) + getDouble(a)));
            }
		
            if (isVector(a) && isVector(b)) {
		array va = getVector(a);
		array vb = getVector(b);
		for (int i = 0; i < va.size(); i++) {
                	vb.push_back(va[i]);
		}
		main_stack.push(makeVector(vb));
            }
		
            if (isVector(a) && isDouble(b)) {
		array va = getVector(a);
		double db = getDouble(b);
		array temp;
		temp.push_back(db);
		for (int i = 0; i < va.size(); i++) {
			temp.push_back(va[i]);
		}
		main_stack.push(makeVector(temp));
            }
		
            if (isDouble(a) && isVector(b)) {
		double da = getDouble(a);
		array vb = getVector(b);
		vb.push_back(da);
		main_stack.push(makeVector(vb));
            }
		
            return 1;
        }
        default:
            setError(UNKNOWN_COMMAND);
            return 0;
    }
}
double VM::loadValue()
{
    union
    {
        double num;
        unsigned char chars[sizeof(double)];
    } helper;
    
    for (auto i = 0; i < sizeof(double); i++)
    {
        helper.chars[i] = getCommand();
    }
    return helper.num;
}
array VM::loadVector()
{
    yla_cop_type size = getCommand();
    array ret;
    if (size == 0)
    {
        setError(BAD_ARRAY_SIZE);
        return ret;
    }
    for (auto i = 0; i < size; i++)
    {
        ret.push_back(loadValue());
    }
    return ret;
}
string VM::formatArray(array a)
{
    ostringstream stream;
    auto sz = a.size();
    stream << "array(" << sz << ", ";
    for (auto i = 0; i < sz-1; i++) {
        stream << a[i] << ", ";
    }
    stream << a[sz-1] << ")";
    return stream.str();    
}
string VM::formatNumber(double n)
{
    ostringstream stream;
    stream << n;
    return stream.str();
}
string VM::lastOutput()
{
    return _lastOutput;
}
int VM::lastError()
{
    return _lastError;
}
