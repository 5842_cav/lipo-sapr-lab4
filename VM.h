#ifndef VM_H
#define VM_H

#include<stack>
#include<string>

#include "mystackitem.h"

using namespace std;

class VM
{
    public:

        static const int MAGIC_SIZE = 4;
        static const yla_cop_type MAGIC[];
        // коды ошибок
        static const int NO_ERROR = 0; // все ок
        static const int NO_CODE = -1; // программы нет
        static const int PROGRAM_EXCEEDED = -2; // попытались из программы прочесть больше, чем там есть
        static const int DIVISION_BY_ZERO = -3; // попытка деления на ноль
        static const int UNKNOWN_COMMAND = -4; // неизвестный код команды
        static const int TYPE_MISMATCH = -5; // массив там, где должно быть число
        static const int STACK_PULL_FAIL = -6; // пустой стек, когда там что-то ожидается
        static const int WRONG_HEADER = -7; // неверное начало программы
        static const int BAD_ARRAY_SIZE = -8; // ошибка размера массива
        VM();
        VM(program program_to_use);
        int doCommand();
        int run();
        string lastErrorText();
        string lastOutput();
        int lastError();

    private:
        // методы
        int doCommandInternal(yla_cop_type cop);
        void setError(int code);
        bool checkHeader();
        double loadValue();
        array loadVector();
        yla_cop_type getCommand();
        string formatArray(array a);
        string formatNumber(double n);
        // поля
        program loaded_program;
        // stacks to hold data
        stack<myStackItem> main_stack;
        size_t pc = 0;
        int _lastError = NO_ERROR;
        string _lastOutput = "";
};

#endif /* VM_H */

