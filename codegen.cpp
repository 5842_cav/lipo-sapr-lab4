#include "codegen.h"
#include "VM.h"

// добавляет байт команды
void addCommand(program& p, yla_cop_type cmd)
{
    p.push_back(cmd);
}
// добавляет вещественное число
void addNumber(program& p, double value)
{
    union
    {
        double num;
        unsigned char chars[sizeof(double)];
    } helper;
    helper.num = value;
    for (auto i = 0; i < sizeof(double); i++)
    {
        p.push_back(helper.chars[i]);
    }
}
// добавляет проверочные байты в начало программы
void addMagic(program& p)
{
   for (auto i = 0; i < VM::MAGIC_SIZE; i++)
   {
       p.push_back(VM::MAGIC[i]);
   }
}
// добавляет посылание числа в стек
void pushNumber(program& p, double value)
{
    addCommand(p, CPUSH);
    addNumber(p, value);
}
// добавляет посылание массива в стек
void pushVector(program& p, array value)
{
    addCommand(p, CPUSHV);
    addCommand(p, (yla_cop_type)value.size());
    for (auto i = 0; i < value.size(); i++)
    {
        addNumber(p, value[i]);
    }
}