#ifndef CODEGEN_H
#define CODEGEN_H

#include "mystackitem.h"
#include "cop.h"

void addCommand(program& p, yla_cop_type cmd);
void addNumber(program& p, double value);

void addMagic(program& p);

void pushNumber(program& p, double value);
void pushVector(program& p, array value);

#endif /* CODEGEN_H */
