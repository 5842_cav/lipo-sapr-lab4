#ifndef COP_H
#define COP_H

/*
codes of operations
*/
#define CNOP	0x00 // пустая операция
// помещение в стек
#define CPUSH	0x01 // числа
#define CPUSHV  0x02 // массива
// арифметика
#define CADD	0x10
#define CSUB	0x11
#define CMUL	0x12
#define CDIV	0x13

#define COUT	0x20 // вывод текущей вершины стека в строку

#define CHALT	0xff // завершение работы ВМ

#endif /* COP_H */

