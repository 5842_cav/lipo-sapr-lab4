#include <iostream>
#include "cop.h"
#include "mystackitem.h"
#include "VM.h"
#include "codegen.h"
#include "yla_test.h"

using namespace std;

YLATEST_SUITE_BEGIN(test_suite)
    YLATEST_ADD_TEST(test_test)
    YLATEST_ADD_TEST(test_vm)
    YLATEST_ADD_TEST(test_calculation)
YLATEST_SUITE_END

int main() {
    // запускаем тесты
    return test_suite();
}

