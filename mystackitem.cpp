#include "mystackitem.h"

myStackItem makeDouble(double value) {
	myStackItem ret = {};
	ret.type = NUMBER;
	ret.d_value = value;
	return ret;
}

myStackItem makeVector(array value) {
	myStackItem ret = {};
	ret.type = VECTOR;
	ret.v_value = value;
	return ret;
}

bool isDouble(myStackItem toTest) {
	return toTest.type == NUMBER;
}

bool isVector(myStackItem toTest) {
	return toTest.type == VECTOR;
}

double getDouble(myStackItem from) {
	return from.d_value;
}
array getVector(myStackItem from) {
	return from.v_value;
}