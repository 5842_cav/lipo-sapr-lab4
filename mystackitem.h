#ifndef _mystackitem_h
#define _mystackitem_h

#include <vector>
using namespace std;

// почти один в один с третьей лабы

typedef unsigned char yla_cop_type;

typedef vector<double> array;
typedef vector<yla_cop_type> program;

typedef enum {
	NUMBER,
	VECTOR
} dataType;

typedef struct {
	dataType type;
	double d_value;
	array v_value;
} myStackItem;

myStackItem makeDouble(double value);
myStackItem makeVector(array value);

bool isDouble(myStackItem toTest);
bool isVector(myStackItem toTest);

double getDouble(myStackItem from);
array getVector(myStackItem from);

#endif