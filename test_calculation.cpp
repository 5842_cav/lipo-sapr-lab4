#include "yla_test.h"
#include "codegen.h"
#include "VM.h"

static int positive_plus_test()
{
    // 2 3 + = 5
    program p1;
    addMagic(p1);
    pushNumber(p1, 2);
    pushNumber(p1, 3);
    addCommand(p1, CADD);
    addCommand(p1, COUT);
    addCommand(p1, CHALT);
    
    // 2 3 4 + + = 9
    program p2;
    addMagic(p2);
    pushNumber(p2, 2);
    pushNumber(p2, 3);
    pushNumber(p2, 4);
    addCommand(p2, CADD);
    addCommand(p2, CADD);
    addCommand(p2, COUT);
    addCommand(p2, CHALT);
    
    // 2 3 + 4 + = 9
    program p3;
    addMagic(p3);
    pushNumber(p3, 2);
    pushNumber(p3, 3);
    addCommand(p3, CADD);
    pushNumber(p3, 4);
    addCommand(p3, CADD);
    addCommand(p3, COUT);
    addCommand(p3, CHALT);
    
    VM vm1(p1);
    YLATEST_ASSERT_TRUE(vm1.run() == 1, "ожидалось успешное завершение");
    YLATEST_ASSERT_TRUE(vm1.lastOutput() == "5", "ожидался верный ответ");
    
    VM vm2(p2);
    YLATEST_ASSERT_TRUE(vm2.run() == 1, "ожидалось успешное завершение");
    YLATEST_ASSERT_TRUE(vm2.lastOutput() == "9", "ожидался верный ответ");  
    
    VM vm3(p3);
    YLATEST_ASSERT_TRUE(vm3.run() == 1, "ожидалось успешное завершение");
    YLATEST_ASSERT_TRUE(vm3.lastOutput() == "9", "ожидался верный ответ");  
    
    return 0;
}

static int negative_plus_test()
{
    program p;
    addMagic(p);
    pushNumber(p, 5);
    addCommand(p, CADD);
    addCommand(p, CHALT);
    
    VM vm(p);
    
    YLATEST_ASSERT_FALSE(vm.run() == 1, "ожидалось неверное завершение");
    YLATEST_ASSERT_TRUE(vm.lastError() == VM::STACK_PULL_FAIL, "должна быть ошибка STACK_PULL_FAIL");
    
    return 0;
}

static int positive_minus_test()
{
    // 2 3 - = -1
    program p1;
    addMagic(p1);
    pushNumber(p1, 2);
    pushNumber(p1, 3);
    addCommand(p1, CSUB);
    addCommand(p1, COUT);
    addCommand(p1, CHALT);
    
    // 2 3 4 - -  = 3
    program p2;
    addMagic(p2);
    pushNumber(p2, 2);
    pushNumber(p2, 3);
    pushNumber(p2, 4);
    addCommand(p2, CSUB);
    addCommand(p2, CSUB);
    addCommand(p2, COUT);
    addCommand(p2, CHALT);
    
    // 2 3 - 4 - = -5
    program p3;
    addMagic(p3);
    pushNumber(p3, 2);
    pushNumber(p3, 3);
    addCommand(p3, CSUB);
    pushNumber(p3, 4);
    addCommand(p3, CSUB);
    addCommand(p3, COUT);
    addCommand(p3, CHALT);
    
    VM vm1(p1);
    YLATEST_ASSERT_TRUE(vm1.run() == 1, "ожидалось неуспешное завершение");
    YLATEST_ASSERT_TRUE(vm1.lastOutput() == "-1", "ожидался верный ответ");
    
    VM vm2(p2);
    YLATEST_ASSERT_TRUE(vm2.run() == 1, "ожидалось неуспешное завершение");
    YLATEST_ASSERT_TRUE(vm2.lastOutput() == "3", "ожидался верный ответ");  
    
    VM vm3(p3);
    YLATEST_ASSERT_TRUE(vm3.run() == 1, "ожидалось неуспешное завершение");
    YLATEST_ASSERT_TRUE(vm3.lastOutput() == "-5", "ожидался верный ответ");  
    
    return 0;
}

static int negative_minus_test()
{
    program p;
    addMagic(p);
    pushNumber(p, 6);
    addCommand(p, CSUB);
    addCommand(p, CHALT);
    
    VM vm(p);
    
    YLATEST_ASSERT_FALSE(vm.run() == 1, "ожидалось неверное завершение");
    YLATEST_ASSERT_TRUE(vm.lastError() == VM::STACK_PULL_FAIL, "должна быть ошибка STACK_PULL_FAIL");
    
    program p2;
    addMagic(p2);
    pushNumber(p2, 6);
    pushVector(p2, {0, 1});
    addCommand(p2, CSUB);
    addCommand(p2, CHALT);
    
    VM vm2(p2);
    
    YLATEST_ASSERT_FALSE(vm2.run() == 1, "ожидалось неверное завершение");
    YLATEST_ASSERT_TRUE(vm2.lastError() == VM::TYPE_MISMATCH, "должна быть ошибка TYPE_MISMATCH");    
    
    return 0;
}

static int positive_mult_test()
{
    // 2 3 * = 6
    program p1;
    addMagic(p1);
    pushNumber(p1, 2);
    pushNumber(p1, 3);
    addCommand(p1, CMUL);
    addCommand(p1, COUT);
    addCommand(p1, CHALT);
    
    // 2 3 4 * *  = 24
    program p2;
    addMagic(p2);
    pushNumber(p2, 2);
    pushNumber(p2, 3);
    pushNumber(p2, 4);
    addCommand(p2, CMUL);
    addCommand(p2, CMUL);
    addCommand(p2, COUT);
    addCommand(p2, CHALT);
    
    // 2 3 * 4 * = 24
    program p3;
    addMagic(p3);
    pushNumber(p3, 2);
    pushNumber(p3, 3);
    addCommand(p3, CMUL);
    pushNumber(p3, 4);
    addCommand(p3, CMUL);
    addCommand(p3, COUT);
    addCommand(p3, CHALT);
    
    VM vm1(p1);
    YLATEST_ASSERT_TRUE(vm1.run() == 1, "ожидалось неуспешное завершение");
    YLATEST_ASSERT_TRUE(vm1.lastOutput() == "6", "ожидался верный ответ");
    
    VM vm2(p2);
    YLATEST_ASSERT_TRUE(vm2.run() == 1, "ожидалось неуспешное завершение");
    YLATEST_ASSERT_TRUE(vm2.lastOutput() == "24", "ожидался верный ответ");  
    
    VM vm3(p3);
    YLATEST_ASSERT_TRUE(vm3.run() == 1, "ожидалось неуспешное завершение");
    YLATEST_ASSERT_TRUE(vm3.lastOutput() == "24", "ожидался верный ответ");  
    
    return 0;
}

static int negative_mult_test()
{
    program p;
    addMagic(p);
    pushNumber(p, 6.66);
    addCommand(p, CSUB);
    addCommand(p, CHALT);
    
    VM vm(p);
    
    YLATEST_ASSERT_FALSE(vm.run() == 1, "ожидалось неверное завершение");
    YLATEST_ASSERT_TRUE(vm.lastError() == VM::STACK_PULL_FAIL, "должна быть ошибка STACK_PULL_FAIL");
    
    program p2;
    addMagic(p2);
    pushVector(p2, {0, 1});
    pushNumber(p2, 6);
    addCommand(p2, CMUL);
    addCommand(p2, CHALT);
    
    VM vm2(p2);
    
    YLATEST_ASSERT_FALSE(vm2.run() == 1, "ожидалось неверное завершение");
    YLATEST_ASSERT_TRUE(vm2.lastError() == VM::TYPE_MISMATCH, "должна быть ошибка TYPE_MISMATCH");    
    
    return 0;
}

static int positive_div_test()
{
    // 3 2 / = 1.5
    program p1;
    addMagic(p1);
    pushNumber(p1, 3);
    pushNumber(p1, 2);
    addCommand(p1, CDIV);
    addCommand(p1, COUT);
    addCommand(p1, CHALT);
    
    // 3 2 4 / /  = 6
    program p2;
    addMagic(p2);
    pushNumber(p2, 3);
    pushNumber(p2, 2);
    pushNumber(p2, 4);
    addCommand(p2, CDIV);
    addCommand(p2, CDIV);
    addCommand(p2, COUT);
    addCommand(p2, CHALT);
    
    // 3 2 / 4 / = 0.375
    program p3;
    addMagic(p3);
    pushNumber(p3, 3);
    pushNumber(p3, 2);
    addCommand(p3, CDIV);
    pushNumber(p3, 4);
    addCommand(p3, CDIV);
    addCommand(p3, COUT);
    addCommand(p3, CHALT);
    
    VM vm1(p1);
    YLATEST_ASSERT_TRUE(vm1.run() == 1, "ожидалось неуспешное завершение");
    YLATEST_ASSERT_TRUE(vm1.lastOutput() == "1.5", "ожидался верный ответ");
    
    VM vm2(p2);
    YLATEST_ASSERT_TRUE(vm2.run() == 1, "ожидалось неуспешное завершение");
    YLATEST_ASSERT_TRUE(vm2.lastOutput() == "6", "ожидался верный ответ");  
    
    VM vm3(p3);
    YLATEST_ASSERT_TRUE(vm3.run() == 1, "ожидалось неуспешное завершение");
    YLATEST_ASSERT_TRUE(vm3.lastOutput() == "0.375", "ожидался верный ответ");  
    
    return 0;
}

static int negative_div_test()
{
    program p;
    addMagic(p);
    pushNumber(p, -456.6);
    addCommand(p, CDIV);
    addCommand(p, CHALT);
    
    VM vm(p);
    
    YLATEST_ASSERT_FALSE(vm.run() == 1, "ожидалось неверное завершение");
    YLATEST_ASSERT_TRUE(vm.lastError() == VM::STACK_PULL_FAIL, "должна быть ошибка STACK_PULL_FAIL");
    
    program p2;
    addMagic(p2);
    pushVector(p2, {0, 1});
    pushVector(p2, {-2, -1, 0, 1, 2});
    addCommand(p2, CDIV);
    addCommand(p2, CHALT);
    
    VM vm2(p2);
    
    YLATEST_ASSERT_FALSE(vm2.run() == 1, "ожидалось неверное завершение");
    YLATEST_ASSERT_TRUE(vm2.lastError() == VM::TYPE_MISMATCH, "должна быть ошибка TYPE_MISMATCH");    
 
    program p3;
    addMagic(p3);
    pushNumber(p3, 11);
    pushNumber(p3, 0);
    addCommand(p3, CDIV);
    addCommand(p3, CHALT);
    
    VM vm3(p3);
    
    YLATEST_ASSERT_FALSE(vm3.run() == 1, "ожидалось неверное завершение");
    YLATEST_ASSERT_TRUE(vm3.lastError() == VM::DIVISION_BY_ZERO, "ожидалось деление на ноль");  
    
    return 0;
}

static int array_plus_test()
{
    // {1} {2} + = {1, 2}
    program p1;
    addMagic(p1);
    pushVector(p1, {1});
    pushVector(p1, {2});
    addCommand(p1, CADD);
    addCommand(p1, COUT);
    addCommand(p1, CHALT);
    
    // {0, 1} {2} {3, 4, 5} + +  = {0, 1, 2, 3, 4, 5}
    program p2;
    addMagic(p2);
    pushVector(p2, {0, 1});
    pushVector(p2, {2});
    pushVector(p2, {3, 4, 5});
    addCommand(p2, CADD);
    addCommand(p2, CADD);
    addCommand(p2, COUT);
    addCommand(p2, CHALT);
    
    // {1, 2} 3 + = {1, 2, 3}
    program p3;
    addMagic(p3);
    pushVector(p3, {1, 2});
    pushNumber(p3, 3);
    addCommand(p3, CADD);
    addCommand(p3, COUT);
    addCommand(p3, CHALT);
    
    // 1 {2, 3} +  = {1, 2, 3}
    program p4;
    addMagic(p4);
    pushNumber(p4, 1);
    pushVector(p4, {2, 3});
    addCommand(p4, CADD);
    addCommand(p4, COUT);
    addCommand(p4, CHALT);
    
    VM vm1(p1);
    YLATEST_ASSERT_TRUE(vm1.run() == 1, "ожидалось неуспешное завершение");
    YLATEST_ASSERT_TRUE(vm1.lastOutput() == "array(2, 1, 2)", "ожидался верный ответ");
    
    VM vm2(p2);
    YLATEST_ASSERT_TRUE(vm2.run() == 1, "ожидалось неуспешное завершение");
    YLATEST_ASSERT_TRUE(vm2.lastOutput() == "array(6, 0, 1, 2, 3, 4, 5)", "ожидался верный ответ");  
    
    VM vm3(p3);
    YLATEST_ASSERT_TRUE(vm3.run() == 1, "ожидалось неуспешное завершение");
    YLATEST_ASSERT_TRUE(vm3.lastOutput() == "array(3, 1, 2, 3)", "ожидался верный ответ");  
    
    VM vm4(p4);
    YLATEST_ASSERT_TRUE(vm4.run() == 1, "ожидалось неуспешное завершение");
    YLATEST_ASSERT_TRUE(vm4.lastOutput() == "array(3, 1, 2, 3)", "ожидался верный ответ");  
    
    return 0;
}

static int magnum_opus_test()
{
    // 0 {1, .2, 3., 4.5} + -40e-1 .9E+1 * 7 1 - / + = {0, 1, 0.2, 3, 4.5, -6}
    program p;
    addMagic(p);
    pushNumber(p, 0);
    pushVector(p, {1, .2, 3., 4.5});
    addCommand(p, CADD);
    pushNumber(p, -40e-1);
    pushNumber(p, .9e+1);
    addCommand(p, CMUL);
    pushNumber(p, 7);
    pushNumber(p, 1);
    addCommand(p, CSUB);
    addCommand(p, CDIV);
    addCommand(p, CADD);
    addCommand(p, COUT);
    addCommand(p, CHALT);
    
    VM vm(p);
    
    YLATEST_ASSERT_TRUE(vm.run() == 1, "ожидалось неуспешное завершение");
    YLATEST_ASSERT_TRUE(vm.lastOutput() == "array(6, 0, 1, 0.2, 3, 4.5, -6)", "ожидался верный ответ");
    
    return 0;
}

YLATEST_BEGIN(test_calculation)
    YLATEST_ADD_TEST_CASE(positive_plus_test)
    YLATEST_ADD_TEST_CASE(negative_plus_test)
    YLATEST_ADD_TEST_CASE(positive_minus_test)
    YLATEST_ADD_TEST_CASE(negative_minus_test)
    YLATEST_ADD_TEST_CASE(positive_mult_test)
    YLATEST_ADD_TEST_CASE(negative_mult_test)
    YLATEST_ADD_TEST_CASE(positive_div_test)
    YLATEST_ADD_TEST_CASE(negative_div_test)
    YLATEST_ADD_TEST_CASE(array_plus_test)
    YLATEST_ADD_TEST_CASE(magnum_opus_test)
YLATEST_END