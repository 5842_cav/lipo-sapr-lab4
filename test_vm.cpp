#include "yla_test.h"
#include "codegen.h"
#include "VM.h"

static int basic_test()
{
    program p;
    addMagic(p);
    addCommand(p, CNOP);
    addCommand(p, CHALT);
    
    VM vm(p);
    
    YLATEST_ASSERT_TRUE(vm.run() == 1, "ожидалось успешное завершение");
    YLATEST_ASSERT_TRUE(vm.lastError() == VM::NO_ERROR, "ожидалось отсутствие ошибок");
    return 0;
}

static int number_push_test()
{
    program p;
    addMagic(p);
    pushNumber(p, 8);
    addCommand(p, COUT);
    addCommand(p, CHALT);
    
    VM vm(p);
    
    YLATEST_ASSERT_TRUE(vm.run() == 1, "ожидалось успешное завершение");
    YLATEST_ASSERT_TRUE(vm.lastOutput() == "8", "на выходе должно быть то же, что и на входе");
    return 0;
}

static int array_push_test()
{
    program p;
    addMagic(p);
    pushVector(p, {-3, 1.8, 0.5, 2});
    addCommand(p, COUT);
    addCommand(p, CHALT);
    
    VM vm(p);
    
    YLATEST_ASSERT_TRUE(vm.run() == 1, "ожидалось успешное завершение");
    YLATEST_ASSERT_TRUE(vm.lastOutput() == "array(4, -3, 1.8, 0.5, 2)", "на выходе должно быть то же, что и на входе");
    return 0;
}

static int multi_push_test()
{
    program p;
    addMagic(p);
    pushVector(p, {-3, 1.8, 0.5, 2});
    pushNumber(p, 22);
    pushNumber(p, 0);
    addCommand(p, COUT);
    addCommand(p, CHALT);
    
    VM vm(p);
    
    YLATEST_ASSERT_TRUE(vm.run() == 1, "ожидалось успешное завершение");
    YLATEST_ASSERT_TRUE(vm.lastOutput() == "0", "ожидалось последнее добавленное значение");
    return 0;
}

static int empty_code_test()
{
    program p;
    
    VM vm(p);
    
    YLATEST_ASSERT_TRUE(vm.lastError() == VM::NO_CODE, "должна быть ошибка NO_CODE");
    YLATEST_ASSERT_FALSE(vm.run() == 1, "ожидалось неуспешное завершение");
    return 0;
}

static int no_halt_test()
{
    program p;
    addMagic(p);
    pushNumber(p, 10.01);
    addCommand(p, COUT);

    VM vm(p);
    
    YLATEST_ASSERT_FALSE(vm.run() == 1, "ожидалось неуспешное завершение");
    YLATEST_ASSERT_TRUE(vm.lastError() == VM::PROGRAM_EXCEEDED, "должна быть ошибка PROGRAM_EXCEEDED");
    YLATEST_ASSERT_TRUE(vm.lastOutput() == "10.01", "но число на выходе должно быть");
    return 0;
}

static int bad_array_test()
{
    program p;
    addMagic(p);
    addCommand(p, CPUSHV);  
    addCommand(p, 4);
    
    VM vm(p);
    
    YLATEST_ASSERT_FALSE(vm.run() == 1, "ожидалось неуспешное завершение");
    YLATEST_ASSERT_TRUE(vm.lastError() == VM::PROGRAM_EXCEEDED, "слишком много значений");
    
    return 0;
}

YLATEST_BEGIN(test_vm)
   YLATEST_ADD_TEST_CASE(basic_test)
   YLATEST_ADD_TEST_CASE(number_push_test)
   YLATEST_ADD_TEST_CASE(array_push_test)
   YLATEST_ADD_TEST_CASE(multi_push_test)
   YLATEST_ADD_TEST_CASE(empty_code_test)
   YLATEST_ADD_TEST_CASE(no_halt_test)
   YLATEST_ADD_TEST_CASE(bad_array_test)
YLATEST_END